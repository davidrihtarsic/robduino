---
title: "Robotika v tehniki"
#subtitle: "Delovni zvezek za osnovnošolski predmet"
author: "dr. David Rihtaršič"
date: 2019-09-06 14:47
classoption: onecolumn 			# [onecolumn, twocolumn]
titlepage: true
fontsize: 12pt
#geometry: "top=1cm, bottom=1cm, left=1cm, right=1cm"
numbersections: true
#documentclass: article 		# [article, book, report]
#csl: ieee.csl
#bibliography: bibtex.bib
lang: sl 				# [sl, en-US, us-GB]
template: skripta			# [eisvogel, skripta, raw] 
listings: true

eqnPrefix: ""
figPrefix: ""
secPrefix: "pogl."
codeBlockCaptions: true
lolTitle: "Kazalo Nalog:"

---
\newpage
\tableofcontents
\newpage

# Robotika v tehniki


## Temeljne značilnosti računališko krmiljenih strojev in naprav

[Naslednja stran](./2-Značilnosti_robota.md)

## Značilnosti robota

čalsdkjfčlask dčlaksj dčflkajs čdlfkj ačsldkfj

# Pregled računalniško krmiljenih strojev in naprav

to do...


# Sistem z eno prostostno stopnjo

Če se robot premika v eni dimenziji je to ena prostostna stopnja.

    Število prostostnih stopenj pomeni, koliko neodvisnih motorjev lahko krmilimo na robotu.

> To pa bi bil komentar, ki si jih morate zapomniti ali pa naloge, ki jih morate rešiti
> Naloga:\
> - naredi to in to...

# Krmiljenje s povratno povezavo

File README.md bi mi bil všeč, ker bi se takoj prikazako nekaj besedila in bi nudil nek uvod v to poglavje.

REcimo, da bi v vsakem poglavju izvedel katere UC bi pokrili.  Naprimer takole:

1. Potreba po krmiljenju
2. mehansko stikalo
3. Digitalni vhod

ali pa takole:

> - Lastnosti svetlobnih vrat
> - Linearni potenciometer
